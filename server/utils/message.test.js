const expect = require('expect');

var {generateMessage, generateLocationMessage} = require('./message');

describe('generate message', () => {
	it('should generate the correct message object', () => {
		var from = 'Sam';
		var text = 'New message';
		var message = generateMessage(from, text);

		expect(message).toMatchObject({from,text});
		expect(typeof message.createdAt).toBe('number');
	});
});

describe('generate location message', () => {
	it('should generate correct location object', () => {
		var from = 'Dave';
		var lat = 12;
		var long = 12;
		var message = generateLocationMessage(from, lat, long);

		expect(message).toMatchObject({
			from,
			url: `https://www.google.com/maps?q=${lat},${long}`
		});
		expect(typeof message.createdAt).toBe('number');
	});
})