const expect = require('expect');
const {Users} = require('./users');

describe('Users', () => {
	var users;

	beforeEach(() => {
		users = new Users();
		users.users = [{
			id: '1',
			name: 'Jen',
			room: 'Cupcakes'
		}, {
			id: '2',
			name: 'Dave',
			room: 'Cupcakes'
		}, {
			id: '3',
			name: 'Martha',
			room: 'Muffins'
		}];
	});

	it('should add new user', () => {
		var users = new Users();
		var user = {
			id: '123',
			name: 'Ben',
			room: 'Testing'
		};
		var resUser = users.addUser(user.id, user.name, user.room);

		expect(users.users).toEqual([user]);
	});

	it('should remove a user', () => {
		var userId = '1';
		var user = users.removeUser(userId);

		expect(user.id).toBe(userId);
		expect(users.users.length).toBe(2);
	});

	it('should not remove a user', () => {
		var userId = '100';
		var user = users.removeUser(userId);

		expect(user).toBeUndefined();
		expect(users.users.length).toBe(3);
	});

	it('should find user', () => {
		var userId = '2';
		var user = users.getUser(userId);

		expect(user.id).toBe(userId);
	});

	it('should not find user', () => {
		var userId = '100';
		var user = users.getUser(userId);

		expect(user).toBeUndefined();
	});

	it('should return names for Cupcakes', () => {
		var userList = users.getUserList('Cupcakes');

		expect(userList).toEqual(['Jen', 'Dave']);
	});

	it('should return names for Muffins', () => {
		var userList = users.getUserList('Muffins');

		expect(userList).toEqual(['Martha']);
	});
})